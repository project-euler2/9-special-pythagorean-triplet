def find_triplet(sum_triplet):
    for i in range(5, sum_triplet + 1):
        c_square = i ** 2
        for j in range(1,c_square + 1):
            a_square = j
            b_square = (c_square - a_square)
            if (a_square ** 0.5).is_integer():
                if(b_square ** 0.5).is_integer():
                    if (((a_square**0.5) + (b_square**0.5) + (c_square**0.5)) == sum_triplet):
                        print(a_square ** 0.5)
                        print(b_square ** 0.5)
                        print(c_square ** 0.5)
                        return ((a_square**0.5) * (b_square**0.5) * (c_square**0.5)) 

print(find_triplet(1000))